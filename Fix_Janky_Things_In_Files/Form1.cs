﻿using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Fix_Janky_Things_In_Files
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            ExcelPackage.LicenseContext = OfficeOpenXml.LicenseContext.NonCommercial;

        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            using (OpenFileDialog openFileDialog = new OpenFileDialog())
            {
                openFileDialog.Filter = "Office files (*.xlsx;*.xlsm)|*.xlsx;*.xlsm|All files (*.*)|*.*";
                openFileDialog.FilterIndex = 1;
                openFileDialog.RestoreDirectory = true;

                if (openFileDialog.ShowDialog() == DialogResult.OK)
                {
                    fixjankyfilepath.Text = openFileDialog.FileName;
                }
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            ExcelPackage excel_in = new ExcelPackage(new FileInfo(fixjankyfilepath.Text));

            string original_dir = new FileInfo(fixjankyfilepath.Text).DirectoryName;
            string fname = new FileInfo(fixjankyfilepath.Text).Name;

            string temp_dir = Path.Combine(original_dir, "Updated");

            if (!Directory.Exists(temp_dir))
            {
                Directory.CreateDirectory(temp_dir);
            }

            int rs_count = 0;
            int ts_count = 0;
            int Rs_count = 0;
            int Ts_count = 0;
            int ns_count = 0;

            foreach (ExcelWorksheet ws in excel_in.Workbook.Worksheets)
            {
                if (ws.Hidden.ToString() == "Visible")
                {
                    if (ws.Dimension!= null)
                    {
                        for (int row = 1; row < ws.Dimension.Rows + 1; row++)
                        {
                            for (int col = 1; col < ws.Dimension.Columns + 1; col++)
                            {
                                if (ws.Cells[row, col].Value != null)
                                {
                                    string segment = ws.Cells[row, col].Text;


                                    MatchCollection rs = Regex.Matches(segment, "\r");
                                    MatchCollection ts = Regex.Matches(segment, "\t");
                                    MatchCollection ns = Regex.Matches(segment, "\n\n");

                                    MatchCollection Rs = Regex.Matches(segment, @"\\CR");
                                    MatchCollection Ts = Regex.Matches(segment, @"\\TAB");

                                    //protect \r and \t 
                                    if (radioButton1.Checked == true)
                                    {
                                        rs_count = rs_count + rs.Count;
                                        ts_count = ts_count + ts.Count;
                                        segment = segment.Replace("\r", "\\CR");
                                        segment = segment.Replace("\t", "\\TAB");
                                    }

                                    //remove \r and \t
                                    if (radioButton2.Checked == true)
                                    {
                                        rs_count = rs_count + rs.Count;
                                        ts_count = ts_count + ts.Count;
                                        segment = segment.Replace("\u005F\u000D", "");
                                        segment = segment.Replace("\r", "");
                                        segment = segment.Replace("\t", " ");
                                        
                                    }

                                    if (radioButton3.Checked == true)
                                    {
                                        Rs_count = Rs_count + Rs.Count;
                                        Ts_count = Ts_count + Ts.Count;

                                        segment = segment.Replace("\\CR", "\r");
                                        segment = segment.Replace("\\TAB", "\t");
                                    }

                                    if (radioButton4.Checked == true)
                                    {
                                        ns_count = ns_count + ns.Count;
                                        segment = segment.Replace("\n\n", "\r\n");
                                    }


                                    ws.Cells[row, col].Value = segment.TrimEnd();
                                }
                            }
                        }
                    }
                }
            }


            string new_file = Path.Combine(temp_dir, fname);

            if (radioButton1.Checked == true)
            {

                excel_in.SaveAs(new FileInfo(new_file.Replace(".xls", "_prepped.xls")));

                if (rs_count > 0 && ts_count == 0)
                {
                    MessageBox.Show($"{rs_count} - \"\\r\" Carriage-Returns converted to \"\\CR\"\n\nPlease add \\\\CR to your filter!");
                }
                if (rs_count == 0 && ts_count > 0)
                {
                    MessageBox.Show($"{ts_count} - \"\\t\" Tab characters converted to \"\\TAB\"\n\nPlease add \\\\TAB to your filter!");
                }
                if (rs_count > 0 && ts_count > 0)
                {
                    MessageBox.Show($"{rs_count} - \"\\r\" Carriage-Returns converted to \\\\CR\n{ ts_count} - \"\\t\" Tab characters converted to \\\\TAB\n\nPlease add \\\\CR and \\\\TAB to your filter!");
                }

                Process.Start(new FileInfo(new_file.Replace(".xls", "_prepped.xls")).DirectoryName);
            }

            if (radioButton2.Checked == true)
            {
                excel_in.SaveAs(new FileInfo(new_file.Replace(".xls", "_cleaned.xls")));

                if (rs_count > 0 && ts_count == 0)
                {
                    MessageBox.Show($"{rs_count} - \"\\r\" Carriage-Returns removed");
                }
                if (rs_count == 0 && ts_count > 0)
                {
                    MessageBox.Show($"{ts_count} - \"\\t\" Tab characters removed");
                }
                if (rs_count > 0 && ts_count > 0)
                {
                    MessageBox.Show($"{rs_count} - \"\\r\" Carriage-Returns removed\n{ ts_count} - \"\\t\" Tab characters removed");
                }

                Process.Start(new FileInfo(new_file.Replace(".xls", "_cleaned.xls")).DirectoryName);
            }

            if (radioButton3.Checked == true)
            {
                excel_in.SaveAs(new FileInfo(new_file.Replace(".xls", "_finalized.xls")));
                if (Rs_count > 0 && Ts_count == 0)
                {
                    MessageBox.Show($"{Rs_count} - \"\\r\" Carriage-Returns breaks restored");
                }
                if (Rs_count == 0 && Ts_count > 0)
                {
                    MessageBox.Show($"{Ts_count} - \"\\t\" Tab characters restored");
                }
                if (Rs_count > 0 && Ts_count > 0)
                {
                    MessageBox.Show($"{Rs_count} - \"\\r\" Carriage-Returns restored\n{ Ts_count} - \"\\t\" Tab characters restored");
                }
                Process.Start(new FileInfo(new_file.Replace(".xls", "_finalized.xls")).DirectoryName);
            }


            if (radioButton4.Checked == true)
            {
                excel_in.SaveAs(new FileInfo(new_file.Replace(".xls", "_finalized.xls")));
                MessageBox.Show($"{ns_count} - Double lines breaks converted to Single lines");
                Process.Start(new FileInfo(new_file.Replace(".xls", "_finalized.xls")).DirectoryName);
            }

            if(radioButton5.Checked == true)
            {
                excel_in.SaveAs(new FileInfo(new_file.Replace(".xls", "_resaved.xls")));
                MessageBox.Show($"Resaved!");
                Process.Start(new FileInfo(new_file.Replace(".xls", "_resaved.xls")).DirectoryName);
            }

            
        }


    }
}
